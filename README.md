# Création de pods auto-réparateur avec des politiques de redémarrage

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


### Introduction à la création de pods auto-réparateurs avec des politiques de redémarrage

Bonjour et bienvenue dans cette leçon où nous allons parler de la création de pods auto-réparateurs grâce aux politiques de redémarrage. Voici un aperçu rapide des sujets que nous allons aborder :

1. Les politiques de redémarrage
2. Les différentes politiques de redémarrage disponibles dans Kubernetes : Always, OnFailure, et Never
3. Une démonstration pratique des politiques de redémarrage

### Qu'est-ce que les politiques de redémarrage ?

Kubernetes peut redémarrer automatiquement les conteneurs lorsqu'ils échouent. Nous avons déjà parlé de l'utilisation des sondes pour détecter les défaillances des conteneurs. Les politiques de redémarrage contrôlent ce qui se passe une fois qu'une défaillance est détectée. Elles permettent de personnaliser le comportement des redémarrages automatiques des conteneurs défaillants ou même de spécifier que certains conteneurs ne doivent pas être redémarrés automatiquement.

Les politiques de redémarrage sont un composant important des applications auto-réparatrices. Ces applications se réparent automatiquement lorsqu'un problème survient, souvent en redémarrant les conteneurs défaillants. Il existe trois politiques de redémarrage possibles dans Kubernetes : Always, OnFailure et Never. Explorons chacune de ces politiques.

### Les politiques de redémarrage

1. **Always** :
   La politique de redémarrage Always est la politique par défaut. Si aucune politique n'est définie, c'est celle-ci qui sera appliquée. Avec cette politique, les conteneurs sont toujours redémarrés s'ils s'arrêtent ou deviennent défaillants. Même si un conteneur se termine avec succès (c'est-à-dire avec un code de sortie réussi), il sera redémarré. Cette politique est utilisée pour les applications qui doivent toujours être en cours d'exécution.

2. **OnFailure** :
   La politique de redémarrage OnFailure redémarre le conteneur uniquement s'il devient défaillant ou s'il se termine avec un code d'erreur. Si une application est conçue pour s'exécuter une seule fois avec succès puis s'arrêter, cette politique empêche le redémarrage du conteneur en cas de succès, mais le redémarrera en cas d'échec.

3. **Never** :
   La politique de redémarrage Never est l'opposée de la politique Always. Quel que soit le résultat, succès ou échec, le conteneur ne sera jamais redémarré. Cette politique est utilisée pour les applications qui doivent s'exécuter une seule fois sans être automatiquement redémarrées.

### Démonstration pratique

Passons maintenant à une démonstration pratique pour explorer ces politiques de redémarrage dans un cluster Kubernetes.

1. **Création d'un pod avec la politique de redémarrage Always** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: always-pod
   spec:
     containers:
     - name: busybox
       image: busybox
       args: ["sh", "-c", "sleep 10"]
       restartPolicy: Always
   ```

   Cette configuration redémarrera toujours le conteneur, même s'il se termine avec succès.

2. **Création d'un pod avec la politique de redémarrage OnFailure** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: onfailure-pod
   spec:
     containers:
     - name: busybox
       image: busybox
       args: ["sh", "-c", "sleep 10"]
       restartPolicy: OnFailure
   ```

   Cette configuration redémarrera le conteneur uniquement s'il échoue.

3. **Création d'un pod avec la politique de redémarrage Never** :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: never-pod
   spec:
     containers:
     - name: busybox
       image: busybox
       args: ["sh", "-c", "sleep 10"]
       restartPolicy: Never
   ```

   Cette configuration ne redémarrera jamais le conteneur, quel que soit le résultat.

### Conclusion

Pour récapituler, nous avons discuté des politiques de redémarrage dans Kubernetes. Nous avons exploré les politiques Always, OnFailure et Never, et nous avons réalisé une démonstration pratique pour montrer comment ces politiques fonctionnent dans un cluster Kubernetes. C'est tout pour cette leçon. À la prochaine !


# Reférences



https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#restart-policy